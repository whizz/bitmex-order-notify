"use strict";

require("dotenv").config();

const BitMEXClient = require("bitmex-realtime-api");
const bitmexClient = new BitMEXClient({
  testnet: process.env.TESTNET ? true : false,
  apiKeyID: process.env.APIKEY,
  apiKeySecret: process.env.APISECRET,
  maxTableLen: 1,
});

const Telegraf = require("telegraf");
const bot = new Telegraf(process.env.TELEGRAM_TOKEN);

const send = (text) => {
  bot.telegram.sendMessage(process.env.TELEGRAM_CHAT_ID, text);
};

const processOrder = (message) => {
  message.forEach((data) => {
    if (data.ordStatus === "Filled") {
      const content =
        `${data.ordType} order to ${data.side} ${data.orderQty}` +
        ` ${data.symbol} for ${data.price} filled`;
      send(content);
      console.log(`Sent notification: ${content}`);
    }
  });
};

const processInstrument = (message) => {
  message.forEach((data) => {});
};

const log = (...args) => {
  console.log(new Date(), ...args);
}

bitmexClient.on("error", console.error);
bitmexClient.on("open", () => log("Connection opened."));
bitmexClient.on("close", () => log("Connection closed."));
bitmexClient.on("initialize", () => log("Client initialized."));
bitmexClient.addStream(process.env.SYMBOL || "XBTUSD", "order", processOrder);
bitmexClient.addStream(process.env.SYMBOL || "XBTUSD", "instrument", processInstrument);

let prevPrice = 0;
setInterval(()=> {
  const lastPrice = bitmexClient.getTable("instrument").XBTUSD[0].lastPrice;
  const delta = prevPrice ? lastPrice - prevPrice : 0;
  prevPrice = lastPrice;
  log("Last price:", lastPrice, "Delta:", delta);
}, 60000);