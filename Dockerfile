FROM node:13-alpine
WORKDIR /usr/local/src
COPY package*.json ./
RUN npm install --only=production
COPY . ./
CMD [ "npm", "start" ]
